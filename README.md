# PingData iOS SDK

## Installing the PingData iOS SDK framework using CocoaPods

The following steps assumes there is an existing app using cocoapods as the dependency manager.

To install the pingdata SDK to an iOS target add the following line to the top of the Podfile, below the platform line:

```Podfile
source 'https://bitbucket.org/pingdata/pingdatapodspecrepo.git'
```

Then add the following lines in any targets that will use the pingdatasdk:

```Podfile
pod 'PingDataSDK', '~> 1.0.9'
```

Here is an example Podfile for an app that uses the pingdata sdk:

```Podfile
# Uncomment the next line to define a global platform for your project
platform :ios, '9.0'
source 'https://bitbucket.org/pingdata/pingdatapodspecrepo.git'
source 'https://github.com/CocoaPods/Specs.git'

target 'PingDataSDKExample' do
    # Uncomment the next line if you're using Swift or would like to use dynamic frameworks
    # use_frameworks!
    # Pods for PingDataSDK
    pod 'PingDataSDK', '~> 1.0.9'
end
```

Once the PingDataSDK dependency is added, run `pod update && pod install` to pull it in

## Using the PingData SDK functionality

The ping data SDK exposes functionality to uplift receipt data using advanced image processing algorithms. In order to do so there are two primary types:

```Objective-C
@interface PingDataUplifter : NSObject

/**
* Primary functionality to uplift receipt data using the ping data SDK.
* @param receiptData Receipt data retrieved for a receipt.
* @param images Array of UIImage* representing images for the receipt.
*/
- (void) upliftReceiptData:(NSDictionary*) receiptData withImages:(NSArray*) images;


/**
* Delegate to handle uplifted receipt data.
*/
@property id <PingDataUplifterDelegate> delegate;

@end
```

and

```
typedef enum UplifterStitchStatus {
    STITCH_SUCCESSFUL = 1,
    STITCH_CONCATENATED = 2,
    STITCH_UNSUCCESSFUL = 3,
} UplifterStitchStatus;
@protocol PingDataUplifterDelegate <NSObject>

/**
* Delegate method called upon logo fingerprinting completed to return
* the uplifted data.
*/
- (void) finishedLogoFingerprinting:(NSMutableDictionary*) receiptInfo andError:(NSError*)error;

/**
* Delegate method called upon stitching images completed to return stitched receipt.
*/
- (void) finishedProcessingImages: (NSArray*) images withResult: (UIImage*) image andStatus: (UplifterStitchStatus) andError:(NSError*)error;

@end
```

In order to use the Ping Data uplifter first import the ping data framework header:

```
#import <PingDataiOSSDK/PingDataiOSSDK.h>
```

A `PingDataUplifterDelegate` must be implemented that implements the above two delegate methods. The below snipped assumes
  - The delegate is implemented on the current instant
  - A variable called `images` exists, with all image paths
  - A variable called `receptData` exists, with the receipt data to be enhanced

To use the delegate and uplifter the following code snippet can be used:

```Objective-C
PingDataUplifter* pingDataUplifter = [[PingDataUplifter alloc] init];
pingDataUplifter.delegate = self;
[pingDataUplifter upliftReceiptData:receiptData withImagePaths:images];
```

A complete working example is checked into the branch 'example'. To see the key differences outlined in this doc diff this branch against master `git diff master example`. To run a minimal working example (it simply logs when stitching has completed in the delegate methods)

  1. Checkout the example branch
  2. Run pod install
  3. Open workspace file in xcode, and run in a simulator

The app should show a stitched image + the following approximate logs
```
2018-06-05 15:05:29.159128+1000 PingDataTutorialApp[6983:329109] Finished logo fingerprinting
Now I'm stitching 4 images
Erosion complete. Stitching.
Image 0 Image size: 954, 1477 - 16
Image 1 Image size: 954, 1477 - 16
Image 2 Image size: 954, 1477 - 16
Image 3 Image size: 954, 1477 - 16
Processed index 0
Processed index 1
Processed index 2
Processed index 3
2018-06-05 15:05:32.733686+1000 PingDataTutorialApp[6983:329109] Finished stitching images
```
