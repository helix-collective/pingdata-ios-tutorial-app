//
//  ViewController.h
//  PingDataTutorialApp
//
//  Created by Anuraag Sridhar on 31/5/18.
//  Copyright © 2018 Anuraag Sridhar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *mainImageView;

@end

