//
//  ViewController.m
//  PingDataTutorialApp
//
//  Created by Anuraag Sridhar on 31/5/18.
//  Copyright © 2018 Anuraag Sridhar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    NSMutableArray* images = [[NSMutableArray alloc] init];
    NSString* image1 = [resourcePath stringByAppendingPathComponent:@"Image-1.jpg"];
    [images addObject:image1];
    NSString* image2 = [resourcePath stringByAppendingPathComponent:@"Image-2.jpg"];
    [images addObject:image2];
    NSString* image3 = [resourcePath stringByAppendingPathComponent:@"Image-3.jpg"];
    [images addObject:image3];
    NSString* image4 = [resourcePath stringByAppendingPathComponent:@"Image-4.jpg"];
    [images addObject:image4];
    
    //TODO: add code to run ping uplifter on the images here.
    
    //TODO: delete this line in the final app.
    UIImage* image1Image = [UIImage imageWithContentsOfFile:image1];
    self.mainImageView.image = image1Image;
}


- (void) receivedImage: (UIImage*) image {
    self.mainImageView.image = image;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
