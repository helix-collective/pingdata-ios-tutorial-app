//
//  AppDelegate.h
//  PingDataTutorialApp
//
//  Created by Anuraag Sridhar on 31/5/18.
//  Copyright © 2018 Anuraag Sridhar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

