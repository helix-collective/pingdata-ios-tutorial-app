//
//  main.m
//  PingDataTutorialApp
//
//  Created by Anuraag Sridhar on 31/5/18.
//  Copyright © 2018 Anuraag Sridhar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
